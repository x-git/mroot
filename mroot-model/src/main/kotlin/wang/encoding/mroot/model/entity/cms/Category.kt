/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]
<http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>
<http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.model.entity.cms


import com.baomidou.mybatisplus.activerecord.Model
import com.baomidou.mybatisplus.annotations.TableField
import com.baomidou.mybatisplus.annotations.TableId
import com.baomidou.mybatisplus.annotations.TableName
import com.baomidou.mybatisplus.enums.IdType
import org.apache.commons.collections.CollectionUtils
import org.hibernate.validator.constraints.Range
import java.io.Serializable
import java.math.BigInteger
import java.util.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Past
import javax.validation.constraints.Pattern


/**
 * 文章分类实体类
 *
 * @author ErYang
 */
@TableName("cms_category")
class Category : Model<Category>(), Serializable {

    companion object {

        private const val serialVersionUID = -2522621869196482698L

        /* 属性名称常量开始 */

        // -------------------------------------------------------------------------------------------------

        /**
         * 分类ID
         */
        const val ID: String = "id"

        /**
         * 标识
         */
        const val NAME: String = "name"

        /**
         * 名称
         */
        const val TITLE: String = "title"


        // -------------------------------------------------------------------------------------------------

        /* 属性名称常量结束 */

        /**
         * 现有的对象赋值给一个新的对象
         *
         * @param category  Category
         * @return Category
         */
        fun copy2New(category: Category): Category {
            val newCategory = Category()
            newCategory.id = category.id
            newCategory.type = category.type
            newCategory.pid = category.pid
            newCategory.name = category.name
            newCategory.title = category.title
            newCategory.audit = category.audit
            newCategory.allow = category.allow
            newCategory.show = category.show
            newCategory.sort = category.sort
            newCategory.status = category.status
            newCategory.remark = category.remark
            newCategory.gmtCreate = category.gmtCreate
            newCategory.gmtCreateIp = category.gmtCreateIp
            newCategory.gmtModified = category.gmtModified
            return newCategory
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * list 转为 tree list
         *
         * @param list list
         * @return tree list
         */
        fun list2Tree(list: List<Category>?): List<Category>? {
            if (null == list) {
                return null
            }
            val roots: List<Category> = listRoot(list)
            val notRoots: List<*> = CollectionUtils.subtract(list, roots) as List<*>
            for (root: Category in roots) {
                @Suppress("UNCHECKED_CAST")
                root.childrenList = listChildren(root, notRoots as List<Category>)
            }
            return roots
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 找到根节点
         *
         * @param allNodes allNodes
         * @return list 集合
         */
        private fun listRoot(allNodes: List<Category>): List<Category> {
            val results = ArrayList<Category>()
            for (node: Category in allNodes) {
                val isRoot: Boolean = allNodes.none { node.pid == it.id }
                if (isRoot) {
                    results.add(node)
                }
            }
            return results
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 根据根节点找到子节点
         *
         * @param root     root
         * @param allNodes allNodes
         * @return list 集合
         */
        private fun listChildren(root: Category,
                                 allNodes: List<Category>): List<Category> {
            val children = ArrayList<Category>()
            for (comparedOne: Category in allNodes) {
                if (comparedOne.pid == root.id) {
                    comparedOne.parentRule = root
                    children.add(comparedOne)
                }
            }
            if (allNodes.isNotEmpty()) {
                val notChildren: List<*> = CollectionUtils.subtract(allNodes, children) as List<*>
                for (child: Category in children) {
                    @Suppress("UNCHECKED_CAST")
                    val tmpChildren: List<Category> = listChildren(child, notChildren as List<Category>)
                    child.childrenList = tmpChildren
                }
            }
            return children
        }

        // -------------------------------------------------------------------------------------------------

    }


    /**
     * 分类ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    var id: BigInteger? = null

    // -------------------------------------------------------------------------------------------------

    /**
     * 1主分类2次分类3分类
     */
    @NotNull(message = "validation.type.range")
    @Range(min = 1, max = 4, message = "validation.type.range")
    var type: Int? = null

    // -------------------------------------------------------------------------------------------------

    /**
     * 父级分类
     */
    @Range(min = 0, message = "validation.cms.category.pid.range")
    var pid: BigInteger? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 标识
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,80}$", message = "validation.name.pattern")
    var name: String? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 名称
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,80}$", message = "validation.title.pattern")
    var title: String? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 发布的文章是否需要审核，1是需要；2是不需要
     */
    @NotNull(message = "validation.cms.category.audit.range")
    @Range(min = 1, max = 2, message = "validation.cms.category.audit.range")
    var audit: Int? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 是否允许发布内容，1允许；2不允许
     */
    @NotNull(message = "validation.cms.category.allow.range")
    @Range(min = 1, max = 2, message = "validation.cms.category.allow.range")
    var allow: Int? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 是否前台可见，1是前后台都可见；2是后台可见
     */
    @NotNull(message = "validation.cms.category.show.range")
    @Range(min = 1, max = 2, message = "validation.cms.category.show.range")
    var show: Int? = null

    // -------------------------------------------------------------------------------------------------


    /**
     * 排序
     */
    @Range(min = 0, message = "validation.sort.range")
    var sort: Long? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 状态(1是正常，2是禁用，3是删除)
     */
    @NotNull(message = "validation.status.range")
    @Range(min = 1, max = 3, message = "validation.status.range")
    var status: Int? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 备注
     */
    @Pattern(regexp = "^[a-zA-Z0-9，。、\\u4e00-\\u9fa5]{0,200}$", message = "validation.remark.pattern")
    var remark: String? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 创建时间
     */
    @Past(message = "validation.gmtCreate.past")
    var gmtCreate: Date? = null


    // -------------------------------------------------------------------------------------------------


    /**
     * 创建IP
     */
    @Pattern(regexp = "^[0-9.]{6,50}\$", message = "validation.gmtCreateIp.pattern")
    var gmtCreateIp: String? = null


    // -------------------------------------------------------------------------------------------------

    /**
     * 修改时间
     */
    @Past(message = "validation.gmtModified.past")
    var gmtModified: Date? = null


    // -------------------------------------------------------------------------------------------------

    /**
     * 父级权限
     */
    @TableField(exist = false)
    var parentRule: Category? = null
    /**
     * 子级权限
     */
    @TableField(exist = false)
    var childrenList: List<Category>? = null

    // -------------------------------------------------------------------------------------------------

    override fun pkVal(): BigInteger? {
        return this.id
    }

    // -------------------------------------------------------------------------------------------------

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Category

        if (id != other.id) return false
        if (type != other.type) return false
        if (pid != other.pid) return false
        if (name != other.name) return false
        if (title != other.title) return false
        if (audit != other.audit) return false
        if (allow != other.allow) return false
        if (show != other.show) return false
        if (sort != other.sort) return false
        if (status != other.status) return false
        if (remark != other.remark) return false
        if (gmtCreate != other.gmtCreate) return false
        if (gmtCreateIp != other.gmtCreateIp) return false
        if (gmtModified != other.gmtModified) return false
        if (parentRule != other.parentRule) return false
        if (childrenList != other.childrenList) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (type ?: 0)
        result = 31 * result + (pid?.hashCode() ?: 0)
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (title?.hashCode() ?: 0)
        result = 31 * result + (audit ?: 0)
        result = 31 * result + (allow ?: 0)
        result = 31 * result + (show ?: 0)
        result = 31 * result + (sort?.hashCode() ?: 0)
        result = 31 * result + (status ?: 0)
        result = 31 * result + (remark?.hashCode() ?: 0)
        result = 31 * result + (gmtCreate?.hashCode() ?: 0)
        result = 31 * result + (gmtCreateIp?.hashCode() ?: 0)
        result = 31 * result + (gmtModified?.hashCode() ?: 0)
        result = 31 * result + (parentRule?.hashCode() ?: 0)
        result = 31 * result + (childrenList?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "Category(id=$id, type=$type, pid=$pid, name=$name, title=$title, audit=$audit, allow=$allow, show=$show, sort=$sort, status=$status, remark=$remark, gmtCreate=$gmtCreate, gmtCreateIp=$gmtCreateIp, gmtModified=$gmtModified, parentRule=$parentRule, childrenList=$childrenList)"
    }


    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End Category class

/* End of file Category.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/model/entity/cms/Category.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
