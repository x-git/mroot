package wang.encoding.mroot.common.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * 防止重复提交注解 用于方法上
 * 在新建页面方法上 设置 init 为 true 此时拦截器会在 session 中保存一个 token
 * 同时需要在新建的页面中添加 <input type="hidden" name="formToken" value="${formToken}">
 * 保存方法需要验证重复提交的 设置 remove 为 true
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FormToken {

    boolean init() default false;

    // -------------------------------------------------------------------------------------------------

    boolean remove() default false;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End Token interface

/* End of file Token.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/admin/common/annotation/Token.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
