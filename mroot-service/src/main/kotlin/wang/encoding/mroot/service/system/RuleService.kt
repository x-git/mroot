/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system

import wang.encoding.mroot.common.service.BaseService
import wang.encoding.mroot.model.entity.system.Rule
import java.math.BigInteger
import java.util.*
import kotlin.collections.ArrayList


/**
 * 权限 service
 *
 * @author ErYang
 */
interface RuleService : BaseService<Rule> {

    /**
     * 初始化新增 Rule 对象
     *
     * @param rule Rule
     * @return Rule
     */
    fun initSaveRule(rule: Rule): Rule

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化修改 Rule 对象
     *
     * @param rule Rule
     * @return Rule
     */
    fun initEditRule(rule: Rule): Rule

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     */
    fun validationRule(rule: Rule): String?

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 权限
     *
     * @param rule Rule
     * @return ID  BigInteger
     */
    fun saveBackId(rule: Rule): BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 权限
     *
     * @param rule Rule
     * @return ID  BigInteger
     */
    fun updateBackId(rule: Rule): BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 权限 (更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    fun removeBackId(id: BigInteger): BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复 权限 (更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    fun recoverBackId(id: BigInteger): BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 url 地址查询权限
     *
     * @param url 地址
     * @return Rule
     */
    fun getByUrl(url: String): Rule?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 名称 查询权限
     *
     * @param title 名称
     * @return Rule
     */
    fun getByTitle(title: String): Rule?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据角色 id 查询权限
     *
     * @param roleId 角色id
     * @return Set<Rule>
     */
    fun listByRoleId(roleId: BigInteger): TreeSet<Rule>?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据角色 id 查询权限 id
     *
     * @param roleId 角色id
     * @return id 集合字符串
     */
    fun list2RoleByRoleIdAndPidGt0(roleId: BigInteger): List<Rule>?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据角色 id 查询权限 id
     *
     * @param roleId 角色id
     * @return id 集合字符串
     */
    fun listIdByRoleId2String(roleId: BigInteger): String?

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 pid 大于 0、类型小于 4 权限集合
     *
     * @return List<Rule>
     */
    fun listPidGt0AndTypeLt4(): List<Rule>?

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 pid 大于 0 权限集合
     *
     * @return List<Rule>
     */
    fun listPidGt0(): List<Rule>?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色 id 和 权限 id 批量新增 角色-权限 表
     *
     * @param roleId BigInteger
     * @param ruleIdArray Array<BigInteger>
     *
     * @return Int
     */
    fun saveBatchByRoleIdAndRuleIdArray(roleId: BigInteger, ruleIdArray: Array<BigInteger>): Int?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色 id 和 权限 id 批量删除 角色-权限 表
     *
     * @param roleId BigInteger
     * @param ruleIdArray Array<BigInteger>
     *
     * @return Int
     */
    fun removeBatchByRoleIdAndRuleIdArray(roleId: BigInteger, ruleIdArray: Array<BigInteger>?): Int?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色id 删除 角色-权限
     *
     * @param roleId 角色id
     * @return Int
     */
    fun removeByRoleId(roleId: BigInteger): Int?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色id 删除 角色-权限
     *
     * @param roleIdArray Array<BigInteger>
     * @return Int
     */
    fun removeByRoleIdArray(roleIdArray: ArrayList<BigInteger>?): Int?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 pid 查询权限
     *
     * @param pid BigInteger
     * @return List<Rule>
     */
    fun listByPid(pid: BigInteger): List<Rule>?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 pid 查询 权限集合
     *
     * @param pidArray  ArrayList<BigInteger>
     * @return List<Rule>
     */
    fun listByPidArray(pidArray: ArrayList<BigInteger>?): List<Rule>?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 查询 Rule  缓存
     *
     * @param id ID
     * @return Config
     */
    fun getById2Cache(id: BigInteger): Rule?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 移除 Rule 缓存
     *
     * @param id BigInteger
     *
     */
    fun removeCacheById(id: BigInteger)

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据  id 批量移除 Rule 缓存
     *
     * @param idArray ArrayList<BigInteger>
     *
     */
    fun removeBatchCacheById(idArray: ArrayList<BigInteger>)

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RuleService class

/* End of file RuleService.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/system/RuleService.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
