/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.cms


import com.baomidou.mybatisplus.plugins.Page
import wang.encoding.mroot.model.entity.cms.Article
import wang.encoding.mroot.common.service.BaseService

import java.math.BigInteger

/**
 * 后台 文章 Service 接口
 *
 * @author ErYang
 */
interface ArticleService : BaseService<Article> {


    /**
     * 初始化新增 Article 对象
     *
     * @param article Article
     * @return Article
     */
    fun initSaveArticle(article: Article): Article

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化修改 Article 对象
     *
     * @param article Article
     * @return Article
     */
    fun initEditArticle(article: Article): Article

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     */
    fun validationArticle(article: Article): String?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 Article
     *
     * @param title 名称
     * @return Article
     */
    fun getByTitle(title: String): Article?

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 文章
     *
     * @param article Article
     * @return ID  BigInteger
     */
    fun saveBackId(article: Article): BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 文章
     *
     * @param article Article
     * @return ID  BigInteger
     */
    fun updateBackId(article: Article): BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 文章 (更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    fun removeBackId(id: BigInteger): BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复 文章 (更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    fun recoverBackId(id: BigInteger): BigInteger?

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param page   Page<Article>
     * @param orderByField   排序字段
     * @param isAsc  是否正序 默认正序
     * @param article   Article
     * @return Page<Article>
     */
    fun list2BlogPage(page: Page<Article>, article: Article, orderByField: String? = "",
                      isAsc: Boolean = true): Page<Article>?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id  更新阅读量
     *
     * @param id BigInteger
     *
     */
    fun updateView(id: BigInteger)

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据分类ID查询 Article
     *
     * @param categoryId BigInteger
     * @return List<Article>
     */
    fun listByCategoryId(categoryId: BigInteger): List<Article>?

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 categoryId 查询 文章集合
     *
     * @param categoryIdArray  ArrayList<BigInteger>
     * @return List<Article>
     */
    fun listByCategoryIdArray(categoryIdArray: ArrayList<BigInteger>?): List<Article>?

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ArticleService interface

/* End of file ArticleService.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/cms/Article.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
