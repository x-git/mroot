/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.cms.impl


import com.baomidou.mybatisplus.plugins.Page
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import wang.encoding.mroot.common.service.BaseServiceImpl
import wang.encoding.mroot.common.util.HibernateValidationUtil
import wang.encoding.mroot.model.entity.cms.Article
import wang.encoding.mroot.model.enums.StatusEnum
import org.springframework.stereotype.Service
import wang.encoding.mroot.mapper.cms.ArticleMapper
import wang.encoding.mroot.service.cms.ArticleService

import java.math.BigInteger
import java.time.Instant
import java.util.Date
import java.util.HashMap


/**
 * 后台 文章 Service 接口实现类
 *
 * @author ErYang
 */
@Service
class ArticleServiceImpl : BaseServiceImpl
<ArticleMapper, Article>(), ArticleService {

    companion object {
        /**
         * logger
         */
        private val logger: Logger = LoggerFactory.getLogger(ArticleServiceImpl::class.java)
    }

    /**
     * 初始化新增 Article 对象
     *
     * @param article Article
     * @return Article
     */
    override fun initSaveArticle(article: Article): Article {
        article.status = StatusEnum.NORMAL.key
        article.gmtCreate = Date.from(Instant.now())
        return article
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化修改 Article 对象
     *
     * @param article Article
     * @return Article
     */
    override fun initEditArticle(article: Article): Article {
        article.gmtModified = Date.from(Instant.now())
        return article
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     */
    override fun validationArticle(article: Article): String? {
        return HibernateValidationUtil.validateEntity(article)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 article
     *
     * @param title 名称
     * @return Article
     */
    override fun getByTitle(title: String): Article? {
        val article = Article()
        article.title = title
        return super.getByModel(article)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 文章
     *
     * @param article Article
     * @return ID  BigInteger
     */
    override fun saveBackId(article: Article): BigInteger? {
        val id: Int? = super.save(article)
        if (null != id && 0 < id) {
            return article.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 文章(更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    override fun removeBackId(id: BigInteger): BigInteger? {
        val article = Article()
        article.id = id
        article.status = StatusEnum.DELETE.key
        article.gmtModified = Date.from(Instant.now())
        val backId: Int? = super.remove2StatusById(article)
        if (null != backId && 0 < backId) {
            return id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复 文章 (更新状态)
     *
     * @param id BigInteger
     * @return ID  BigInteger
     */
    override fun recoverBackId(id: BigInteger): BigInteger? {
        val article = Article()
        article.id = id
        article.status = StatusEnum.NORMAL.key
        article.gmtModified = Date.from(Instant.now())
        val backId: Int? = super.recover2StatusById(article)
        if (null != backId && 0 < backId) {
            return id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 文章
     *
     * @param article Article
     * @return ID  BigInteger
     */
    override fun updateBackId(article: Article): BigInteger? {
        val flag: Boolean = super.updateById(article)
        if (flag) {
            return article.id
        }
        return null
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param page   Page<Article>
     * @param orderByField   排序字段
     * @param isAsc  是否正序 默认正序
     * @param article   Article
     * @return Page<Article>
     */
    override fun list2BlogPage(page: Page<Article>, article: Article, orderByField: String?,
                               isAsc: Boolean): Page<Article>? {
        if (null != orderByField && orderByField.isNotBlank()) {
            page.orderByField = orderByField
        }
        page.isAsc = isAsc
        page.records = superMapper?.list2BlogPage(page, article)
        return page
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id  更新阅读量
     *
     * @param id BigInteger
     *
     */
    override fun updateView(id: BigInteger) {
        val params = HashMap<String, Any>()
        params["id"] = id
        params["gmtModified"] = Date.from(Instant.now())
        superMapper!!.updateView(params)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据分类ID查询 Article
     *
     * @param categoryId BigInteger
     * @return List<Article>
     */
    override fun listByCategoryId(categoryId: BigInteger): List<Article>? {
        val article = Article()
        article.categoryId = categoryId
        //article.status = StatusEnum.NORMAL.key
        return superMapper!!.listByT(article)
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 categoryId 查询 文章集合
     *
     * @param categoryIdArray  ArrayList<BigInteger>
     * @return List<Article>
     */
    override fun listByCategoryIdArray(categoryIdArray: ArrayList<BigInteger>?): List<Article>? {
        val params = HashMap<String, Any>()
        if (null != categoryIdArray && categoryIdArray.isNotEmpty()) {
            params["categoryIdArray"] = categoryIdArray
        }
        return superMapper?.listByCategoryIdArray(params)
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ArticleServiceImpl class

/* End of file ArticleServiceImpl.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/service/cms/impl/ArticleServiceImpl.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
