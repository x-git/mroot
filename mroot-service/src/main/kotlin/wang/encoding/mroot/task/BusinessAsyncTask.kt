/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.task


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Async
import org.springframework.scheduling.annotation.AsyncResult
import org.springframework.stereotype.Component
import wang.encoding.mroot.model.entity.cms.Article
import wang.encoding.mroot.model.entity.cms.ArticleContent
import wang.encoding.mroot.model.entity.system.User
import wang.encoding.mroot.service.cms.ArticleContentService
import java.math.BigInteger
import java.util.concurrent.Future


/**
 * 异步调用
 *
 * @author ErYang
 */
@Component
class BusinessAsyncTask {


    @Autowired
    private lateinit var articleContentService: ArticleContentService


    /**
     * 根据 文章 新增 文章内容
     *
     * @param article Article
     *
     * @param articleContent ArticleContent
     *
     * @return Future<String>
     */
    @Async
    fun addArticleContent(article: Article, articleContent: ArticleContent): Future<String> {
        articleContent.id = article.id
        val articleContentSave: ArticleContent = articleContentService.initSaveArticleContent(articleContent)
        articleContentService.saveBackId(articleContentSave)
        return AsyncResult(">>>>>>>>addArticleContent执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 文章 更新 文章内容
     *
     * @param article Article
     *
     * @param articleContent ArticleContent
     *
     * @return Future<String>
     */
    @Async
    fun editArticleContent(article: Article, articleContent: ArticleContent): Future<String> {
        articleContent.id = article.id
        articleContentService.updateBackId(articleContent)
        return AsyncResult(">>>>>>>>editArticleContent执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

    /**
     *  删除 文章内容 （更新状态）
     *
     * @param article Article
     *
     * @return Future<String>
     */
    @Async
    fun removeArticleContent(article: Article): Future<String> {
        articleContentService.removeBackId(article.id!!)
        return AsyncResult(">>>>>>>>removeArticleContent执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

    /**
     *  批量删除 文章内容 （更新状态）
     *
     * @param idArray List<Any>
     *
     * @return Future<String>
     */
    @Async
    fun removeBatchArticleContent2UpdateStatus(idArray: List<Any>): Future<String> {
        articleContentService.removeBatch2UpdateStatus(idArray)
        return AsyncResult(">>>>>>>>removeBatchArticleContent2UpdateStatus执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

    /**
     *  恢复 文章内容
     *
     * @param article Article
     *
     * @return Future<String>
     */
    @Async
    fun recoverArticleContent(article: Article): Future<String> {
        articleContentService.removeBackId(article.id!!)
        return AsyncResult(">>>>>>>>recoverArticleContent执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

    /**
     *  批量恢复 文章内容
     *
     * @param idArray List<Any>
     *
     * @return Future<String>
     */
    @Async
    fun recoverBatchArticleContent2UpdateStatus(idArray: List<Any>): Future<String> {
        articleContentService.recoverBatch2UpdateStatus(idArray)
        return AsyncResult(">>>>>>>>recoverBatchArticleContent2UpdateStatus执行完毕<<<<<<<<")
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BusinessAsyncTask class

/* End of file BusinessAsyncTask.kt */
/* Location: ./src/main/kotlin/wang/encoding/mroot/task/BusinessAsyncTask.kt */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------------

